import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { HomeModule } from './home/home.module';
import { NgModule } from '@angular/core';
import { ShellService } from './shell/shell.service';

const routes: Routes = [
  ShellService.childRoutes(
    [
      { path: 'home', loadChildren: () => HomeModule }
    ]),
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
