import { Route, Routes } from '@angular/router';

import { AuthenticationGuard } from './../core/authentication/authentication.guard';
import { Injectable } from '@angular/core';
import { ShellComponent } from './shell.component';

@Injectable({
  providedIn: 'root'
})
export class ShellService {

  static childRoutes(routes: Routes): Route {
    return {
      path: '',
      component: ShellComponent,
      children: routes,
      canActivate: [AuthenticationGuard],
      data: { reuse: true } // Reuse ShellComponent instance when navigating between child views
    };
  }
}
