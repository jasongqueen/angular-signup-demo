import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService, Credentials } from './../../core/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  error: string;
  credentials: Credentials;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.credentials = this.authenticationService.credentials;
  }

  logout(){
    this.authenticationService.logout().subscribe(
      credentials => {
        // log.debug(`${credentials.username} successfully logged in`);
        this.router.navigate(['signup', { replaceUrl: true }])
      },
      error => {
        // log.debug(`Login error: ${error}`);
        this.error = error;
      }
    );

  }

}
