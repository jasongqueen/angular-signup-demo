import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import { ShellService } from '../shell/shell.service';

const routes: Routes = [
  ShellService.childRoutes([
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent, data: { title: 'Home' } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
