import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { SignupComponent } from './signup.component';

const routes: Routes = [{ path: 'signup', component: SignupComponent, data: { title: 'signup' } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SignupRoutingModule {}
