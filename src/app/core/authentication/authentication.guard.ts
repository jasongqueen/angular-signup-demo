import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from './authentication.service';
import { Injectable } from '@angular/core';
// import { Logger } from '../logger.service';
import { Observable } from 'rxjs';

// const log = new Logger('AuthenticationGuard');

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(private router: Router, private authenticationService: AuthenticationService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authenticationService.isAuthenticated()) {
      return true;
    }

    // log.debug('Not authenticated, redirecting and adding redirect url...');
    this.router.navigate(['/signup'], { queryParams: { redirect: state.url }, replaceUrl: true });

    return false;
  }
}
